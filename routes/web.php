<?php

use App\Jobs\email_rem;
use Illuminate\Support\Facades\Route;
use PharIo\Manifest\Email;
use Illuminate\Support\Facades\Mail;
use App\Mail\emailmailable;
use App\Mail\welcomemail;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::resource('/teacher','tea_controller');
Route::resource('/tests','std');


Route::get('/email',function(){
    $obj=(new email_rem())->delay(Carbon::now()->addseconds(5));
    dispatch($obj);
    return new welcomemail();
});
route::get('index','std@online');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/students',function(){
   return view('test');
});
