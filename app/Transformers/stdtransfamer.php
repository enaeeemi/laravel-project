<?php

use App\stdtransfamer;
use App\student;
use League\Fractal\TransformerAbstract;

class stdtransfamerTransformer extends TransformerAbstract
{
    /**
     * Turn stdtransfamer object into custom array.
     *
     * @param stdtransfamer $stdtransfamer
     * @return array
     */
    public function transform(student $request)
    {
        return [
          'name'=>$request->name,
          'lastname'=>$request->lastname
        ];
    }
}
