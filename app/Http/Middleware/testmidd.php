<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Carbon\Carbon;
use Cache;
use App\User;
use Illuminate\Support\Facades\Redis;

class testmidd
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::check()){
            if(Redis::get('user-is-online')){ 
                $arr[]=Cache::get('user-is-online');
                $key = Auth::user()->id;
           
                  array_push($arr,$key);
               $expire=Carbon::now()->addMinutes(1);
               Cache::put('user-is-online',$arr, $expire); 
            }else{

                $expire=Carbon::now()->addMinutes(1);
                $key = Auth::user()->id;
                Cache::put('user-is-online',$key, $expire);
            }
            
        }
        return $next($request);
    }
    // public function handle($request, Closure $next, ...$guards)
    // {
    //     if(auth()->check()) {
    //         $expiresAt = \Carbon\Carbon::now()->addMinutes(5);
    //         \Cache::put(
    //             'online-users-' . auth()->user()->id, 
    //             auth()->user(), 
    //             $expiresAt
    //         );
    //     }
    // }
}

