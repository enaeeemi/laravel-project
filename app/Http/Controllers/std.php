<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redis;
use App\Http\Requests\stdresquest;
use Illuminate\Http\Request;
use App\student;
use App\Transformers\stdtransfamer;
// use Barryvdh\Debugbar\Facade as Debugbar;
use Spatie\Fractal\Fractal;
use App\Http\Resources\students;
use App\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Arr;
use Auth;
use PHPUnit\Util\Json;

class std extends Controller
{
 
    

public function index(){

    $data = student::whereDate('created_at', Carbon::today())->get();;
    $today = array();
    $time =date('y-m-d');
    


        
    
        
    
        
 

    // foreach($var as $values){

    //     if($values['today']==Carbon::today()){

    //        $var[]=$values['today'];
     $var[]=$data->where('created_at',Carbon::today());
     
        
        return response()->json($var);
    // }
}


public function store(stdresquest $request){
    
$std=new student();
$std->name=$request->std_name;
$std->lastname=$request->std_lastname;
$std->gender=$request->std_gender;
$std->save();

$data=student::all();

return view('student',compact('data'));


}



public function show($id){

 $data=student::find($id);
 return $data;
}

public function destroy($id){

$obj=student::find($id);
$obj->delete();
return redirect('tests');

}

public function edit($id)
{
    $std=student::find($id);
    return view('update_student',compact('std'));

}


public function update(Request $request, $id)
{
    
    $std=student::find($id);
    $std->name=$request->std_name;
    $std->lastname=$request->std_lastname;
    $std->gender=$request->std_gender;
    $std->save();
    
    $data=student::all();
    
    return view('student',compact('data'));
    

}














public function online(){
    $array[]=Cache::get('user-is-online');
    if (!is_array($array)) { 
        return FALSE; 
      } 
      $result = array(); 
      foreach ($array as $key => $value) { 
        if (is_array($value)) { 
          $result = array_merge($result, array_flatten($value)); 
        } 
        else { 
          $result[$key] = $value; 
        } 
      }
      $final=array_unique($result);
      return $final;
}}
    


?>