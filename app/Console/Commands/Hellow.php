<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Hellow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hellow {name=Enayatullah} {--L|lastname=Naeemi}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this commond is just for the test';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $firsname=$this->argument('name');
        $lastname=$this->option('lastname');
     $this->info($firsname.'  '.$lastname);
    }
}
